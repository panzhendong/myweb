<?php
	class Pages extends CI_Controller{

		/*
		*Home Page http://test.local/
		*this allow you to load example: http://test.local/pages/view/$1
		*the $1 parameter will redirect you to views/pages/{$page}.php
		*/
		public function view($page='home'){

			if(!file_exists('application/views/pages/'.$page.'.php')){
				show_404();
			}

			$this->load->view('templates/header');
			$this->load->view('templates/navbar');
			$this->load->view('pages/'.$page);
			$this->load->view('templates/footer');
		}

		/*
		*Just a test to load page in views/test.php
		*/
		public function test()
		{
			$this->load->view('test');
		}
	
}
?>
