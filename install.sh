#!/bin/bash

#install package if not installed, upgrade it if upgrade available
#arg1: package name
#arg2: upgrade if upgrade available, default: yes, optional
install_package() {
    local package_name=$1
    local upgrade_package=$2

    if [ -z "$package_name" ]; then
        echo "Invalid package name given!"
        return
    fi

    #check package status
    dpkg -l "$package_name" &> /dev/null

    if [ $? -eq 0 ]; then
        if [[ "$upgrade_package" != "no" ]]; then
            apt-get install -y --only-upgrade $package_name
        fi
    else
        apt-get install -y $package_name
    fi
}

# import GPG key and PPA
sudo rm /etc/apt/sources.list.d/mongodb-org-3.0.*
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

sudo add-apt-repository -y -r ppa:ondrej/php5
sudo add-apt-repository -y ppa:ondrej/php5-5.6

# update package list
apt-get update

# install or upgrade packages
install_package mongodb-org
install_package apache2
install_package php5
install_package php5-mongo
install_package php5-oauth
install_package php5-apcu
install_package php5-json
install_package php5-mysqlnd
install_package php5-mcrypt 
install_package php5-curl
install_package php5-gearman

#phpunit
which phpunit
if [ $? -eq 0 ]; then
    sudo phpunit --self-upgrade
else
    sudo curl -L https://phar.phpunit.de/phpunit.phar -o /usr/local/bin/phpunit
    sudo chmod a+x /usr/local/bin/phpunit
fi

#config
sudo a2enmod rewrite
sudo a2enmod ssl
sudo service apache2 restart

